import MovieCard from "@/components/movieCard";
import { Button } from "@/components/ui/button";
import Link from "next/link";

interface IMovie {
  title: string;
  name:string;
  image: string;
  overview: string;
  original_title: string;
  release_date: string;
  first_air_date: string;
  backdrop_path: string;
  original_name: string;
  id:number;
  media_type:string;
}

export default async function Home({searchParams}:{searchParams:{page:number}}) {
  /* https://api.themoviedb.org/3/trending/all/week */

  const res = await fetch(`https://api.themoviedb.org/3/trending/all/week?page=${searchParams.page ? searchParams.page : 1}`, {
    headers: {
      Authorization:
        "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJhMzkxYzNhMjQ0YzUxMmM2ZjA3NDZhNTZhZjlmZjFjMiIsInN1YiI6IjY0MTczZDIyMzEwMzI1MDBjNWFhMGQ4MyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.BvoE9hUaOQLzQ-I0F6PDBfplO9o2NgB3yGxYlapy90E",
    },
  });
  const data = await res.json();
  console.log(data, "Data from API");

  return (
    <section className="container mx-auto text-center py-12">
      <h1 className="text-3xl mb-5">Movie Rec!</h1>

      <div className="flex flex-wrap justify-between">
        {data.results.map((item: IMovie, index:number) => (
          <MovieCard
            title={item.title ? item.title : item.name}
            image={item.backdrop_path}
            overview={item.overview}
            original_title={
              item.original_title ? item.original_title : item.original_name
            }
            release_date={
              item.release_date ? item.release_date : item.first_air_date
            }
            key={index}
            id = {item.id}
            type={item.media_type}
          />
        ))}
      </div>
      <div className="flex justify-between">
        {Number(searchParams.page) > 1 ?<Button asChild>
          <Link href={`/?page=${Number(searchParams.page) - 1}`}>
            Previous Page
          </Link>
        </Button>: <Button disabled>
            Previous Page
        </Button>}
        
        <Button asChild>
          <Link href={`/?page=${searchParams.page ? Number(searchParams.page) + 1 : 1 + 1}`}>
            Next Page
          </Link>
        </Button>
      </div>
    </section>
  );
}
