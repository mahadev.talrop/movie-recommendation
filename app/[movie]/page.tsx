import Image from "next/image";

export default async function Movie({
  params,
  searchParams,
}: {
  params: { movie: number };
  searchParams: { type: string };
}) {
  let fetch_url =
    searchParams.type == "tv"
      ? "https://api.themoviedb.org/3/tv/"
      : "https://api.themoviedb.org/3/movie/";
  const res = await fetch(`${fetch_url}${params.movie}`, {
    headers: {
      Authorization:
        "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJhMzkxYzNhMjQ0YzUxMmM2ZjA3NDZhNTZhZjlmZjFjMiIsInN1YiI6IjY0MTczZDIyMzEwMzI1MDBjNWFhMGQ4MyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.BvoE9hUaOQLzQ-I0F6PDBfplO9o2NgB3yGxYlapy90E",
    },
  });
  const data = await res.json();

  const colours = [
    "bg-pink-700",
    "bg-purple-900",
    "bg-sky-800",
    "bg-teal-700",
    "bg-emerald-400",
    "bg-lime-300",
    "bg-amber-500",
  ];
  function get_random(list: Array<string>) {
    return list[Math.floor(Math.random() * list.length)];
  }

  console.log(data, "Data from api");
  return (
    <section className="container text-center py-12">
      <section className="relative">
        <div className="w-full h-[500px] bg-gray-950/[.06]">
          <Image
            src={`https://image.tmdb.org/t/p/original${data.backdrop_path}`}
            alt={data.title}
            width={500}
            height={500}
            className="w-full block h-full opacity-60"
          ></Image>
        </div>
        <div className="absolute bottom-[30px] left-[30px]">
          <h2 className="text-4xl text-left">
            {searchParams.type == "tv" ? data.name : data.title}
          </h2>
          <h4 className="text-2xl text-left italic  my-5">
            {searchParams.type == "tv"
              ? data.original_name
              : data.original_title}
          </h4>
          <h3 className="text-left">
            Type : {searchParams.type == "tv" ? "TV Series" : "Movie"}
          </h3>
        </div>
      </section>
      <section className="py-5">
        <h3 className="text-3xl text-left bold mb-3">Overview</h3>
        <p className="text-left">{data.overview}</p>
      </section>
      <section className="py-5">
        <h3 className="text-3xl text-left bold mb-3">Genres</h3>
        <div className="flex gap-5">
          {data.genres.map(
            (item: { id: number; name: string }, index: number) => (
              <h5
                key={index}
                className={`${get_random(colours)} py-3 px-5 rounded-lg`}
              >
                {item.name}
              </h5>
            )
          )}
        </div>
      </section>
    </section>
  );
}
