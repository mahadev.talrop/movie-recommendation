import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import Image from "next/image";
import Link from "next/link";

const truncate = (input: string) =>
  input?.length > 300 ? `${input.substring(0, 254)}...` : input;

interface IMovieCard {
  image: string;
  title: string;
  original_title: string;
  overview: string;
  release_date: string;
  id:number;
  type:string;
}

export default function MovieCard({
  image,
  title,
  original_title,
  overview,
  release_date,
  id,
  type
}: IMovieCard) {
  return (
    <Link href={`/${id}?type=${type}`} className="inline-block w-[23%] mb-5">
      <Card className="flex flex-col justify-between">
        <CardHeader>
          <Image
            src={`https://image.tmdb.org/t/p/original${image}`}
            alt={title}
            width={500}
            height={500}
            className="block w-full"
          />
          <CardTitle>{title}</CardTitle>
          <CardDescription>{original_title}</CardDescription>
        </CardHeader>
        <CardContent>
          <p>{truncate(overview)}</p>
        </CardContent>
        <CardFooter>
          <h3>Release Date: {release_date}</h3>
        </CardFooter>
      </Card>
    </Link>
  );
}
